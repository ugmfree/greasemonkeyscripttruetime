// ==UserScript==
// @name        TrueTime2 - Attività
// @version     7.11
// @namespace   TrueTimeNew
// @author      Gianluca Negrelli
// @description Inserimento attività su nuovo truetime
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @include     http*://servizi.ecocerved.it/TrueTime.New/Reserved/Attivita/At*
// @exclude     http*://servizi.ecocerved.it/TrueTime.New/Reserved/Attivita/DettaglioNotaSpese.aspx*
// @include     http*://truetime.ecocerved.it/Reserved/Attivita/At*
// @exclude     http*://truetime.ecocerved.it/Reserved/Attivita/Attivita/DettaglioNotaSpese.aspx*
// @run-at      document-end
// @grant       metadata
// ==/UserScript==

var $ = window.jQuery;

// Modera l'errore TT sulla funziona TextAreaResizer() che va in errore al load della pagina.
// TT non ne viene impattato ma gli script sì
$.fn.TextAreaResizer = function () {};

// Alcune funzioni sono valide solo per il form attivita
var isAttivita = $("form").first().attr('action').toLowerCase().indexOf("attivita.aspx") >= 0 ;

// Fa sparire l'header
$('#ctl00_topheader').hide();

// funzioni js da iniettare sulla pagina
var scriptCode="";

// Script per l'inizializzazione della pagina da richiamare più volte
var initPage = function() {

	// Un po' di attività preliminari per preparare la pagina

	// Rimozione del fieldset
	$('fieldset').contents().unwrap();

	// Resize della tabella centrale
	$('#ctl00_ctMain_FormView1_editAtt_txtAttivita').closest('tr').find('td:first-child').css('width', '50');

	// Resize delle note
	$('#ctl00_ctMain_FormView1_editAtt_txtDescr')
		.css('width', '400')
		.next().css('width', '400');

	// Identifica il primo dei pulsanti per il salvataggio per estrarre il td
	var tdPulsantiSave = $('#ctl00_ctMain_FormView1_btnSaveAndNew').closest('td');
	if($(tdPulsantiSave).length == 0){
		tdPulsantiSave = $('#ctl00_ctMain_FormView1_btnUpdateAndNew').closest('td');
	}

	// Resize della tabella dei pulsanti
	$(tdPulsantiSave).parents('table')
		.css('width', '500px');

	// Sistemazione pulsanti
	$(tdPulsantiSave)
		.css('text-align','left')
		.css('padding-left','65px');

	$(tdPulsantiSave).find('input')
		.css('margin','0 30px 10px 0')
		.css('width', '110px');

	// Aggiunge lo strumento di selezione ore
	if($('#divSelezioneMinuti').length==0){
		var selezionatoreMinuti = "<div id='divSelezioneMinuti' style='display:none;' class='selezionatoriHHmm'><div>0</div><div>15</div><div>30</div><div>45</div>";
		var selezionatoreOre = "<div id='divSelezioneOre' style='display:none;' class='selezionatoriHHmm'><div>0</div><div>1</div><div>2</div><div>3</div><div>4</div><div style='clear:both'>5</div><div>6</div><div>7</div><div>8</div></div>";

		$('<div class="testo" style="clear:both;padding-top:10px;">Seleziona i minuti lavorati</div>' + selezionatoreMinuti).insertAfter('#ricPanel');
		$('<div class="testo" style="clear:both;padding-top:10px;">Seleziona le ore lavorate</div>' + selezionatoreOre).insertAfter('#ricPanel');

		// Inserisce gli attributi
		$('.selezionatoriHHmm div')
		.css('float', 'left')
		.css('margin', '4px')
		.css('text-align','center')
		.css('min-width','28px')
		.css('padding-top','5px')
		.addClass('button')
		.mouseover(function () {
			$(this).css('cursor', 'pointer');
		});
	}

	// Eventi click
	$('#divSelezioneMinuti div').attr('title', 'Clicca per inserire i minuti nella casella Durata dell\'attività')
	.click(function() {
	  $('#ctl00_ctMain_FormView1_editAtt_txtMinuti').val($(this).html());
	});

	$('#divSelezioneOre div')
		.attr('title', 'Clicca per inserire le ore nella casella Durata dell\'attività')
		.click(function() {
			$('#ctl00_ctMain_FormView1_editAtt_txtOre').val($(this).html());
	});

	// Mostra i div per la selezione ore minuti
	$('.selezionatoriHHmm').show();

	// Formatta i textbox per ore e minuti
	$('#ctl00_ctMain_FormView1_editAtt_txtMinuti').css('text-align', 'right');
	$('#ctl00_ctMain_FormView1_editAtt_txtOre').css('text-align', 'right');

	// Drop down attività
	$('#ctl00_ctMain_FormView1_editAtt_ddlTipoAtt')
		.attr('size', 10)
		.css('height', '360px')
		.css('font-size', '11px');
	$('#ctl00_ctMain_FormView1_editAtt_ddlTipoAtt option[value="000"]').remove();;

	// Radio button per i luoghi di esecuzione
  var tableRadioButton = 	$('#ctl00_ctMain_FormView1_editAtt_RadioButtonList1');
  var tdParent = $(tableRadioButton).parent('td');
  $(tdParent).attr('id', 'tdRadioLocations');

	// Spostamento degli input all'interno del TD padre
  $(tableRadioButton).find('td').each(function(i){
	  if(i == 1){
	  	$('</br>').appendTo(this);
	  }
	  $(this).children().appendTo(tdParent);
	});

	// Memorizzazione dell'ultimo stato selezionato
  $(tdParent).find('input').change(function(){
		var ultimaSelezione = $(tdParent).find('input:checked').val();
	 	localStorage.setItem('attivitaUltimaLocation', ultimaSelezione);
	});

	// Scorciatoia per filtrare le attività
	$('#ctl00_ctMain_treeComVis_txtFilter').prop('placeholder', 'CTRL+S per Searchare');
	$(document).keydown(function(e) {
		if(e.ctrlKey && e.key == 's') {
			//console.log('ctrl+s pressed');
			$('#ctl00_ctMain_treeComVis_txtFilter').focus().select();
			e.preventDefault();
			e.stopPropagation();
		}
	});

	// L'ESC sulla textbox di ricerca la svuota e non chiude tuta la treeview
	$('#ctl00_ctMain_treeComVis_txtFilter').keydown(function(e){
		if(e.keyCode == 27){
			$(this).val('');
			e.preventDefault();
			e.stopPropagation();
			return false;
		}
	})

	// Nasconde il calendario ufficiale
	$('img.ui-datepicker-trigger').hide();
	unsafeWindow.$('#ctl00_ctMain_FormView1_editAtt_dataAtt').datepicker('destroy');

	// Riattribuzione dell'ultima data utilizzata nel calendario
	if($('#divDatePicker').val() != ''){
		// Verifica che non si tratti di un edit di una voce già inserita. Nel caso la data non va modifica
 		if($('#ctl00_ctMain_FormView1_editAtt_dataAtt').hasClass('textBox')) {
  		$('#ctl00_ctMain_FormView1_editAtt_dataAtt').val($('#divDatePicker').val());
 			}
	}

	// Nasconde il pulsante per le commesse
	$('.buttonSel').hide();

	// Imposta il default sulle ore lavorate
	if($('#ctl00_ctMain_FormView1_editAtt_txtOre').val() == ''){
		$('#ctl00_ctMain_FormView1_editAtt_txtOre').val('8');
	};

	// Riattribuzione dell'ultima locazione selezionata
	// Verifica che non si tratti di un edit di una voce già inserita. Nel caso la località non va modificata
 	if($('#ctl00_ctMain_FormView1_editAtt_dataAtt').hasClass('textBox')) {
		var ultimaSelezione = localStorage.getItem('attivitaUltimaLocation');
		if(ultimaSelezione == null) ultimaSelezione = 0;
		$('#tdRadioLocations input[value=' + ultimaSelezione + ']').attr('checked', true);
	}

	// Apre il dialog commesse
	writeRootActForm ('', '4', 'ctl00_ctMain_treeComVis_divTreeCommesseJS', 'ctl00$ctMain$treeComVis$lbFake');
	showHideNotSelectables (true);
	var dialogObj = unsafeWindow.$('#ctl00_ctMain_treeComVis_divTreeCommesseJSMain');
	dialogObj.dialog({
		autoOpen: false,
		modal: false,
		resizable: true,
		draggable: true,
		closeText: '',
		title: '',
		width: '500px',
		//height: '70%',
		height:($(window).height()),
		position: 'right top'
	});

	dialogObj.parent().appendTo(jQuery('form:first'));
	dialogObj.dialog('open');
	$('.ui-dialog-titlebar')
		.css('height', '5px')
		.css('margin-top', '0');
	//	$('#ctl00_ctMain_treeComVis_divTreeCommesseJSMain')
	$('.ui-dialog-content')
		.css('padding', '0');
		//.css('overflow', 'hidden');

	$('#divTreeId').children('div')
		.css('margin-left', '2px');
};


// Provvede ad evidenziare le festività
var highlightFestivity = function(month, year){
	//Evidenzia in rosso le festività fisse e mobili
	var highlight = function(){

		var pad = '00';
		month = (pad+month).slice(-pad.length);
		year = year.toString().substring(2,4);

		//Clona l'array delle feste
		var festivitaTotaliAnno = unsafeWindow._FestivitaFisse.slice(0);
		var festivitaMobili = unsafeWindow._FestivitaMobili.slice(0);

		var ricercaFissa = '' + month;
		var ricercaMobile = '' + month + '/' + year;

		//Verifica se aggiungere le festività mobili dell'anno (solo se cadono nel mese e nell'anno correnti)
		for (var i = 0; i < festivitaMobili.length; i++) {
			if (festivitaMobili[i].substring(3, 8) == ricercaMobile)
			{
				festivitaTotaliAnno.push(festivitaMobili[i].substring(0, 5));
			}
		}

		//console.log(festivitaTotaliAnno);

		//Festività fisse
		//Cerca il mese tra le date
		for (var y = 0; y < festivitaTotaliAnno.length; ++y) {

			if (festivitaTotaliAnno[y].substring(3, 5) === ricercaFissa)
			{
				var day = festivitaTotaliAnno[y].substring(0,2);
				if(day.indexOf('0') == 0)
				{
					day = day.substring(1,2);
				}

				unsafeWindow.$('#divDatePicker a').filter(function() {
					return this.innerHTML == day;
				}).parent('td').addClass('ui-datepicker-week-end');
				//.text('XX');
			}
		}
	};

	//La funzione è chiamata dopo 100ms per lasciare il tempo al calendario di aggiornarsi
	setTimeout(highlight, 100);
};

exportFunction(initPage, unsafeWindow, {defineAs: "initPage"});
exportFunction(highlightFestivity, unsafeWindow, {defineAs: "highlightFestivity"});


// Rebind al partial postback
scriptCode+=" \n\
	var prm = Sys.WebForms.PageRequestManager.getInstance(); \n\
	prm.add_endRequest(function() \n\
	{ \n\
		initPage(); \n\
	});";

// Operazioni da fare all'onload
scriptCode+=" \n\
	$(document).ready(function() { \n\
		\n\
		initPage(); \n\
		\n\
		// Memorizza la data precedente se presente \n\
		var dataDefault = new Date(); \n\
		var dataPresente = $('#ctl00_ctMain_FormView1_editAtt_dataAtt').val(); \n\
		if(dataPresente !== ''){ \n\
			dataPresente = dataPresente.split('/'); \n\
			dataDefault = new Date(dataPresente[2], dataPresente[1]-1, dataPresente[0]);  \n\
		}  \n\
		// Mostra un calendario parallelo \n\
		$('#ricPanel').after('<div id=\"divDatePicker\" style=\"clear:both;\"></div>'); \n\
		$('#ricPanel').after('<div class=\"testo\" style=\"clear:both;padding-top:10px;\">Seleziona il giorno</div>'); \n\
		$('#divDatePicker').datepicker({ \n\
			firstDay: 1 , \n\
			altField: '#ctl00_ctMain_FormView1_editAtt_dataAtt', \n\
			dateFormat: 'dd/mm/yy', \n\
			defaultDate: dataDefault, \n\
			onChangeMonthYear: function(year, month, widget) { \n\
				highlightFestivity(month, year); \n\
			} \n\
		}); \n\
		\n\
		// Subito fa il check delle festività per il mese corrente \n\
		var d = new Date(); \n\
		highlightFestivity(d.getMonth() + 1, d.getFullYear()); \n\
	}); ";

var styleCode=" \
	td.ui-datepicker-week-end a {color: #FF4542 !important} \
	td.ui-datepicker-week-end a.ui-state-active {color: #eb8f00 !important;}";

// inietta le funzioni se è la pagina attivita
if(isAttivita){
	var headTag = document.getElementsByTagName("head")[0];

	var scriptNode = document.createElement('script');
	scriptNode.type = 'text/javascript';

	var styleNode = document.createElement('style');
	styleNode.type = 'text/css';

	scriptNode.innerHTML=scriptCode;
	styleNode.innerHTML=styleCode;

	headTag.appendChild(scriptNode);
	headTag.appendChild(styleNode);
}

