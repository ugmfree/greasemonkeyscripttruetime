// ==UserScript==
// @name        TrueTime2 - MeseAttvità
// @version     7.2
// @namespace   TrueTimeNew
// @description Miglioramento fruibilità riepilogo attivita
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @include     http*://servizi.ecocerved.it/TrueTime.New/Reserved/Attivita/Mese*
// @include     http*://truetime.ecocerved.it/Reserved/Attivita/Mese*
// @run-at      document-end
// @grant       metadata
// ==/UserScript==

var TrueTimeHack = TrueTimeHack || {};
TrueTimeHack.MeseAttivita = function(){  

  var self = this;
	
	 self.initialize = function(){
		$.extend($.expr[':'], { 
			'starts-with': function (elem, i, meta) { 
				var text = $.trim($(elem).text()); 
				var toSearch = meta[3]; 
				// first index is 0 
				return text.indexOf(toSearch) === 0; 
			} 
		}); 
		
	 // Chiama le funzione dirette 
	 self.evidenziaDate(); 
	};
	
	
	self.evidenziaDate = function(){
		// Estrai la data odierna 
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; 
		var yy = today.getFullYear(); 
		if (mm < 10) mm = '0' + mm; 
			if (dd < 10) dd = '0' + dd; 
		yy = yy.toString().substring(2); 
		today = dd + '/' + mm + '/' + yy; 
		// Trova le tabelle e gli span 
		var dateVisibiliTable = $('#divAttContainer fieldset table'); 
		var dateVisibiliSpan = $(dateVisibiliTable).find('[id$=\"lblDate\"]'); 

		// Ridefinisce la tabella 
		$(dateVisibiliSpan).css('padding', '2px 5px'); 
		$(dateVisibiliSpan).parent().prev().find('span').css('padding', '2px 5px'); 
		$(dateVisibiliTable) 
			.attr('cellspacing', 0) 
			.attr('cellpadding', 2) 
			.css('border-collapse', 'separate') 
			.css('border-spacing', '0 2px');

	 // Cambia colore al background di tutte le festività segnalate nativamente da truetime 
	 $(dateVisibiliTable).find('div.textBox').each(function(){ 
	 //console.log($(this).css('background-color'));			
			var backColor = $(this).css('background-color');
	     if(backColor == 'red' || backColor == 'rgb(255, 0, 0)'){ 
			 // Arrossisce la linea
			 self.highlightLine($(this), 'red', '#FFDDDD', 'red');
		 } 

		 // Omogeneizza tutti i fondini delle cellette 
			$(this) 
				.css('background-color', 'rgb(250, 240, 200)') 
				.css('color', 'black'); 
			}) 


		// Processa la data odierna e le festività mobili e fisse 
		$(dateVisibiliSpan).each(function(){ 

			// Cerca la data odierna 
			if(today.indexOf($(this).text()) > -1){  
				self.highlightLine(this, 'green', '#DEF4E4', 'green');
			} 

//console.log(unsafeWindow._FestivitaFisse);
			
			// Cerca le festività
			if(unsafeWindow._FestivitaFisse.indexOf($(this).text().substring(0,5)) > -1 || unsafeWindow._FestivitaMobili.indexOf($(this).text()) > -1){ 
				self.highlightLine(this, 'red', 'red', 'white');
				$(this).parent().parent().find('span').css('font-weight', 'bold');
			} 
		}); 
	}

	self.highlightLine = function(spanToProcess, borderColor, backColor, foreColor){  

		var todayLine = $(spanToProcess).parent().parent(); 
		$(todayLine).find('td:lt(4)') 
				.css('border-bottom', '1px solid ' + borderColor) 
				.css('border-top', '1px solid ' + borderColor) 
				.css('background-color', backColor) 
				.find('span').css('color', foreColor); 
		$(todayLine).find('td').first() 
				.css('border-left', '1px solid ' + borderColor); 
		$(todayLine).find('td:lt(4)').last() 
				.css('border-right', '1px solid ' + borderColor); 
	}
};

window.addEventListener('load', function() { new TrueTimeHack.MeseAttivita().initialize();}, true);
		
		
																								 