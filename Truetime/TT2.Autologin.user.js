// ==UserScript==
// @name        TrueTime2 - AutoLogin
// @version     7.8
// @namespace   TrueTimeNew
// @author      Gianluca Negrelli
// @description Auto Login in TrueTime
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @include     http*://servizi.ecocerved.it/TrueTime.New/Login.aspx*
// @include     http*://servizi.ecocerved.it/truetime.new/
// @include     http*://truetime.ecocerved.it
// @include     http*://truetime.ecocerved.it/
// @include     http*://truetime.ecocerved.it/Login.aspx*
// @include     http*://truetime.ecocerved.it//Login.aspx*
// @run-at      document-end
// @grant       none
// ==/UserScript==

// Aggiunge gli elementi necessari alla pagina

$('head').append(`
	<style type="text/css">
		.divScriptMessage { display:none; margin: 20px auto; padding:30px; border: 1px solid silver; width:500px; text-align:center; }
	</style>`);

$('#txtPassword').parents('table').first().append(`
	<tr><td></td><td>
	<div id="divManageCredential" style="display:none;">
		<label><input type="checkbox" id="chkSaveCredential">Ricorda le credenziali</label>
	</div>
	</td></tr>`);

// Pulsante per scaricamento di tutti gli script e info versione
$('#Button1').parent().after(`
	<div style="font-size: 10px; margin:10px; text-align:left;">
		<div style="float:left;">
			Script GreaseMonkey attivi<br/>
			Versione script: <span id="spanVersione"></span>
		</div>
		<div style="float:right";">
			<button id="btnCheckAll" type="button" class="button" title="Da usare durante la prima installazione oppure quando qualcuno degli script è disallineato per qualche motivo">Ottieni tutti gli script</button>
		</div>
	</div>`);

$('body').append(`
	<div id="divScriptOutdated" class="divScriptMessage">
		Sono stati rilasciati nuovi script.<br/>Clicca sui link qui sotto per installarli <br/>
		<span class="spanScript" style="color:#bc494a"></span> <br/>
	</div>`);

$('body').append(`
	<div id="divScriptAll" class="divScriptMessage">
		Questi sono tutti gli script per TT.<br/>Clicca sui link qui sotto per installarli <br/>
		<span class="spanScript" style="color:#bc494a"></span> <br/>
	</div>`);

$('body').append(`
	<div id="divScriptUpdated" class="divScriptMessage">
		Gli script sono aggiornati
	</div>`);

$('body').append(`
	<div id="divCheckingScript" class="divScriptMessage" style="border:none;">
		<img style="margin:10px; width:25px" src="images/loading.gif" alt="Loading"/>
		Checking script....
	</div>`);




// funzioni js
function checkScript(allScript){

  	$('#divScriptOutdated').hide();
  	$('#divScriptUpdated').hide();

  	var printScriptLink = function(data){
		var linkRoot = data.linkRoot;
	  	$('.spanScript').html('');
		for(var i=0; i<data.script.length; i++){
			var newLink = $('<a />', {href : linkRoot + data.script[i], text : data.script[i] });
			$('.spanScript').append(newLink);
			$('.spanScript').append('<br/>');
		}
	};

	var successGet = function(data){
		$('#divCheckingScript').hide();

		data = $.parseJSON(data);

	  	if(allScript){
		  printScriptLink(data);
		  $('#divScriptAll').show();
		}
	  	else{
			var versionReleased = data.version.toString();
		  	if(parseInt(versionReleased.replace('.','')) > parseInt(_CurrentVersion.replace('.','')))
			{
				printScriptLink(data);
				$('#divScriptOutdated').show();
			}
			else
			{
				$('#divScriptUpdated').show();
			}
		}
	};

	$('#divCheckingScript').show();

  	//var checkingUrl = 'https://dl.dropboxusercontent.com/u/1620135/TrueTimeChecker.txt',
	//rawgit è un servizio per poter accedere alle pagina in hosting su github direttamente
	//var checkingUrl = 'https://rawgit.com/epikgit/TrueTime/master/TrueTimeChecker.txt?' + Math.random(),
	//var checkingUrl = 'https://www.ugmfree.it/public/TrueTime/TrueTimeChecker.txt?' + Math.random(),

  	var checkingUrl = 'https://bitbucket.org/ugmfree/greasemonkeyscripttruetime/raw/HEAD/Truetime/TruetimeChecker.txt?' + Math.random();

  	if(allScript){
		checkingUrl = checkingUrl.replace('.txt?', '.all.txt?')
	}

  	$.ajax({
		type: 'GET',
		//contentType: 'application/json; charset=utf-8',
		contentType: 'text/plain',
		dataType: 'text',
		url: checkingUrl,
		async: true,
		cache: false,
		//cache: true,
		success: function(data) { successGet(data); },
		failure: function(data){alert('failure');}
	});
};

// Verifica della password

function loginPassword(){
	if(typeof(Storage) !== 'undefined') {
		// Mostra il checkbox
		$('#divManageCredential').show();

		// Estrae le vecchie credenziali se presenti
		$('#txtUsername').val(localStorage.getItem('username'));
		$('#txtPassword').val(localStorage.getItem('password'));
		if($('#txtPassword').val().length > 0){
			$('#chkSaveCredential').prop('checked', true);
			$('#txtPassword').focus();
		}
		else{
			$('#txtUsername').focus();
		}

		$('#Button1').click(function(){
			var username = '';
			var password = '';
			if($('#chkSaveCredential').is(':checked')){
				username = $('#txtUsername').val();
				password = $('#txtPassword').val();
			}
			localStorage.setItem('username', username);
			localStorage.setItem('password', password);
			return true;
		});
	}
};

// Operazioni da fare all'onload
$(document).ready(function() {
	$('#spanVersione').html(_CurrentVersion);
	$('#btnCheckAll').on('click', function(){checkScript(true);});
	loginPassword();
	setTimeout(function(){checkScript(false);}, 1000);
});



