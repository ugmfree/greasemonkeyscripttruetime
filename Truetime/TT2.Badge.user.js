// ==UserScript==
// @name        TrueTime2 - Badge
// @version     7.2
// @namespace   TrueTimeNew
// @description Miglioramento fruibilità tempi attività svolte
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.js
// @include     http*://servizi.ecocerved.it/TrueTime.New/Reserved/Attivita/Badge*
// @include     http*://truetime.ecocerved.it/Reserved/Attivita/Badge*
// @run-at      document-end
// @grant       metadata
// ==/UserScript==

var TrueTimeHack = TrueTimeHack || {};

TrueTimeHack.BadgeManager = function() {
  var self = this;

  self.initialize = function() {

	// Rimozione postback con collapse buttons per evitare partial postback che richiederebbero il ricalcolo di tutti gli orari
	self.rimozioneRighe();

	// Calcola i timing
	self.processaTabella();

	// Ridefinizione stili
	$('#ctl00_ctMain_grd_badge_DXMainTable tr.dxgvDataRow td.dxgv').css('font-size','10px').css('padding','0 6px');

	// Split della tabella per migliorare la visibilità
	// Crea i due div contenitore
	var divContainer = $( "<div id='div1' style='float:left'></div> <div id='div2' style='float:left'></div> ");
	$(divContainer).appendTo('#ctl00_content');

	var mainTable = $('#ctl00_ctMain_grd_badge_DXMainTable');
	var splitBy = Math.floor($(mainTable).find('tr').length / 2) - 1;
	if(splitBy < 0) return;

	var intestazioneTrovata = false;
	while(!intestazioneTrovata)
	{
	  splitBy += 1;
	  var tr = $(mainTable).find('tr')[splitBy];
	  intestazioneTrovata = tr == undefined || $(tr).hasClass('dxgvGroupRow');
	}

	if(splitBy < $(mainTable).find('tr').length)
	{
	  var rows = mainTable.find('tr').slice(splitBy);
	  var secondTable = $('<table class="dxgvTable"><tbody></tbody></table>');
	  secondTable.find('tbody').append(rows);
	  mainTable.find('tr').slice(splitBy).remove();
	  $(mainTable).appendTo('#div1');
	  $(secondTable).appendTo('#div2');
	}
  };

  self.rimozioneRighe = function() {
	// Rimuove intestazioni
	$('#ctl00_ctMain_grd_badge_grouppanel').closest('div').remove();
	$('#ctl00_ctMain_grd_badge_DXHeadersRow0').remove();

	// Rimuove colonne inutili
	var mainTable = $('#ctl00_ctMain_grd_badge_DXMainTable');
	//$('#ctl00_ctMain_grd_badge_DXMainTable').find('img').attr('onclick','').unbind('click');
	//$('#ctl00_ctMain_grd_badge_DXMainTable td:first-child').remove();
	$(mainTable).find('td:first-child').remove();

	// Rimozione righe vuote
	$(mainTable).find('tr.dxgvDataRow td.dxgv').filter(function() {return $.trim($(this).text()) === '';}).parent('tr').remove();

	// Rimozione righe titolo vuote
	$(mainTable).find('tr.dxgvGroupRow').each(function(){
	  var nextRow = $(this).next('tr');
	  if(nextRow == null || !nextRow.is('tr') || $(nextRow).hasClass('dxgvGroupRow')) $(this).hide();

	});
  };

  self.processaTabella = function() {
	var granTotaleMensile = 0;

	// Rimuove righe per debug
	//$('#ctl00_ctMain_grd_badge_DXMainTable tr:gt(6)').remove();
	$('table.dxgvTable tr.dxgvGroupRow:visible').each(function(index){


	  // Riga di riferimento dell'header
	  var trHeader = $(this);

	  // Cella dove scrivere della riga di riferimento dell'header
	  //var tdHeader = $(this).find('td:eq(1)');
	  var tdHeader = $(this).find('td:first');
	  var currentHeader = tdHeader.html();
	  var currentHeaderDate = currentHeader.substring(6, 6+10);

	  // Estrae le ore consuntivate se presenti
	  //var oreConsuntivate = tdHeader.html().match('Ore Consuntiv:(.*) ');
	  var minutiConsuntivati = null;
	  //var firstIndex = currentHeader.indexOf("{") + 16;
	  //var lastIndex =  currentHeader.indexOf("}");

	  var firstIndex = currentHeader.indexOf("[") + 10;
	  var lastIndex =  currentHeader.indexOf("]");

	  if(lastIndex != firstIndex){

		minutiConsuntivati = currentHeader.substring(firstIndex, lastIndex);
		minutiConsuntivati = self.converToMinutes(minutiConsuntivati);
		//if(currentHeaderDate.substring(0,2)=='09') {alert(minutiConsuntivati);}
		//	alert(currentHeader + ' ' + firstIndex + ' ' + lastIndex);
	  }

	  // Calcola le ore di presenza in millisecondi
	  var calcoloPresenza = self.calcoloTimingMillisecondi(trHeader);

	  if(calcoloPresenza.totalTime.hours() == 0) return;

	  var spanDeltaMinuti = self.createRedBox($('<span/>'));

	  $(spanDeltaMinuti).html('&nbsp;?&nbsp;');
	  var deltaMinutiPositivo = false;

	  if(minutiConsuntivati != null)
	  {
		var deltaMinutiInt = calcoloPresenza.totalTime.minutes() + calcoloPresenza.totalTime.hours() * 60 - minutiConsuntivati;

		$(spanDeltaMinuti).text(deltaMinutiInt);

		if(deltaMinutiInt >= 0){
		  deltaMinutiPositivo = true;

		  spanDeltaMinuti = self.createGreenBox(spanDeltaMinuti);
		  $(spanDeltaMinuti).text('+' + deltaMinutiInt);
		}
		if(deltaMinutiInt == 0){
		  (spanDeltaMinuti).text('-');
		}
		granTotaleMensile += deltaMinutiInt;
	  }

	  // Creazione dell'elemento dove inserire il testo
	  var spanHost = $('<span />')
	  .css('color','green')
	  .css('font-weight', 'bold')
	  .addClass('minuteBox');

	  var htmlToShow = 'Totale ' + calcoloPresenza.totalTime.format("HH:mm");
	  if(calcoloPresenza.totalTime.hours() < 8)
	  {
		if(!deltaMinutiPositivo){
		  $(spanHost).css('color','red');
		}
		if (calcoloPresenza.dateExit != null) {
		  if (moment(currentHeaderDate.replace('/','-'), "DD-MM-YYYY").diff(moment(), "days") == 0)
			htmlToShow += ' - Uscita '  + calcoloPresenza.dateExit.format("HH:mm");
		}
	  }

	  $(spanHost).html(htmlToShow);
	  $(spanHost).append(spanDeltaMinuti);
	  $(tdHeader).find('span.minuteBox').remove();
	  $(tdHeader).append(spanHost);
	});

	// Aggiunge il gran totale
	var tdGranTotale = self.createRedBox($('<td class="minuteBox" />'));

	if(granTotaleMensile > 0){
	  tdGranTotale = self.createGreenBox(tdGranTotale);
	  $(tdGranTotale).text('Minuti mensili: +' + granTotaleMensile);
	}
	else{
	  $(tdGranTotale).text('Minuti mensili: ' + granTotaleMensile);
	}

	var tdHost = $('#ctl00_ctMain_but_Ricerca').parent();
	$(tdHost).removeAttr('align');
	$(tdHost).removeAttr('colspan');

	if($(tdHost).parent().find('td.minuteBox') != undefined){
	  $(tdHost).parent().find('td.minuteBox').remove();
	}
	$(tdHost).parent().prepend(tdGranTotale);
  };

  self.converToMinutes = function(s)  {
	var c = s.split(':');
	//var c = s.split('.');
	return parseInt(c[0]) * 60 + parseInt(c[1]);
  };

  self.createRedBox = function(element)  {
	$(element)
	  .css('margin-left', '3px')
	  .css('background-color','#ffdddd')
	  .css('font-weight', 'bold')
	  .css('color', 'red')
	  .css('border', '1px solid red')
	  .css('padding', '0 3px');
	return element;
  };

  self.createGreenBox = function(element)  {
	element = self.createRedBox(element);
	$(element)
	  .css('color','green')
	  .css('border-color','green')
	  .css('background-color','#def4e4');
	return element;
  };

  self.calcoloTimingMillisecondi = function(trHeader) {
	var totalTime =  { totalTime: moment("2000-01-01 00:00"), dateExit: null };
	var ingresso = '';
	var uscita = '';
	var lastIngresso = '';
	var lastUscita = '';

	var continua = true;

	var trNext = $(trHeader).next();

	//Cicla sulle righe del report
	while (continua)
	{
	  // Verifica se il report è terminato
	  if(trNext.length == 0 || $(trNext).hasClass('dxgvGroupRow'))
	  {
		continua = false;
	  }

	  if(continua){
		var tipologia = $(trNext).find('td:eq(1)').html().toLowerCase();

		if(tipologia.indexOf('ingresso') == 0)
		{
		  if(lastIngresso=='')
		  {
			ingresso = $(trNext).find('td:eq(0)').html();
			lastIngresso = ingresso;
		  }

		  self.addEliminaRiga($(trNext).find('td:eq(1)'));
		}

		if(tipologia.indexOf('uscita') == 0)
		{
		  uscita = $(trNext).find('td:eq(0)').html();
		  lastUscita = uscita;
		  self.addEliminaRiga($(trNext).find('td:eq(1)'));
		  if(lastIngresso.length > 0)
		  {
			lastIngresso = lastIngresso.replace('.',':').replace('.',':');
			uscita = uscita.replace('.',':').replace('.',':');

			// Calcola i millisecondi e resetta le variabili ingresso e uscita
			var uscitaDate = moment("2000-01-01 " + uscita);
			var ingressoDate = moment("2000-01-01 " + lastIngresso);
			var timespan = uscitaDate.subtract({ hours: ingressoDate.hours(), minutes: ingressoDate.minutes() });
			totalTime.totalTime = totalTime.totalTime.add( { hours: timespan.hours(), minutes: timespan.minutes() });
			ingresso = '';
			lastIngresso = '';
			uscita = '';
		  }
		}
		if (lastIngresso.length > 0 && lastUscita.length > 0) {

		  // Quanto manca per fare 8h
		  var differenzaDalle8h = moment("2000-01-01 08:00").subtract({ hours: totalTime.totalTime.hours(), minutes: totalTime.totalTime.minutes() });
		  //console.log("differenzaDalle8h: " + differenzaDalle8h.format());
		  //console.log("differenzaDalle8h: " + differenzaDalle8h.hours());

		  // Calcolo dell'ora di uscita per fare 8 ore
		  lastIngresso = lastIngresso.replace('.',':').replace('.',':');
		  var lastIngressoDate = moment("2000-01-01 " + lastIngresso);
		  //console.log("lastIngressoDate: " + lastIngressoDate.format());
		  totalTime.dateExit = lastIngressoDate.add({ hours: differenzaDalle8h.hours(), minutes: differenzaDalle8h.minutes() });
		  //console.log("totalTime.dateExit: " + totalTime.dateExit.format());
		}
		trNext = $(trNext).next();
	  }
	}

	if((ingresso + uscita + lastIngresso + lastUscita) == ''){
	  // Nessun elemento orario riscontrato si può eliminare anche l'header
	  $(trHeader).remove();
	}
	return totalTime;
  };

  // Rimozione righe comandato dall'utente
  self.addEliminaRiga=function(tdCell){
	if($(tdCell).find('div.btnRemove').length>0)
	{
	  return;
	}

	var buttonElimina = $('<div class="btnRemove" title="Rimuovi la riga" style="color:red; float: right; cursor:pointer;">x</div>');
	$(tdCell).append(buttonElimina);
	$(buttonElimina).click(function(){
	  $(this).closest('tr').remove();
	  self.processaTabella();
	});
  };
};

var badgeModel = new TrueTimeHack.BadgeManager();
window.addEventListener('load', function() { badgeModel.initialize(); }, true);

