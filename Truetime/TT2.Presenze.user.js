// ==UserScript==
// @name        TrueTime2 - Presenze
// @version     7.7
// @namespace   TrueTimeNew
// @description Miglioramento form presenze
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @include     http*://truetime.ecocerved.it/Reserved/Attivita/Report*
// @run-at      document-end
// @grant       metadata
// ==/UserScript==

/*global $:true*/

var TrueTimeHack = TrueTimeHack || {};

TrueTimeHack.PresenzeManager = function () {
	var self = this;
	var tagBtnUncolorize = `
		<div style="float:left">
			<button id="btnUncolorize" type="button" class="dxbButton"
				style="padding: 5px 16px; color:white; font-weight:bold; background-image:linear-gradient(#fff, #000);"
				title="Torna al triste bianco e nero">
				B&W
			</button>
		</div>`;

	self.initialize = function () {

		window.removeEventListener('load', self.initialize);
		self.replaceStyle();

	  	// Monitora i cambiamenti della tabella
		setTimeout(function checkTable() {
			if(!$('#ctl00_ctMain_grd_ReportAdminPresenze_DXMainTable').hasClass('colorizzato')){
				$('#ctl00_ctMain_grd_ReportAdminPresenze_DXMainTable').addClass('colorizzato');
			  	self.colorize();
			}
			setTimeout(checkTable, 200);
		}, 200);

		// Effettua la prima ricerca
		$('#ctl00_ctMain_but_Cerca_I').trigger('click');

		var filtroCustom = function () {
			var inputddl = $('#ctl00_ctMain_grd_ReportAdminPresenze_DXPagerBottom_PSI');

			if (inputddl.val() == undefined) {
				window.setTimeout(filtroCustom, 500);
				return;
			}

			// 50 risultati in tabella
			inputddl.val('50');

			// Rilancia la ricerca
			$('#ctl00_ctMain_grd_ReportAdminPresenze_DXPagerBottom_PSP_DXI2_T').trigger('click');

		  	// Aggiunge il tasto per decolorare le celle
			self.addUncolorizeButton();
			return;
		};

		// Tampina il dom finché non appaiono gli altri controlli
		filtroCustom();
	};

  	// Inserisce le classi css custom
	self.replaceStyle = function () {

		// Defines custom classes
		var cssCustomClass = 'td.dxgvHeader_base {background-color:#d3d3d3; border-image: none; border-style: none none solid; border-width: 0 0 1px; border-color: #d3d3d3; padding: 3px 6px 4px; vertical-align: middle; }';
		cssCustomClass += 'td.dxgvHeader_base td {color:black; text-align:center;}';
		cssCustomClass += 'input.dxeEditArea_base {background-color:#ffffff !important; padding:0}';
		cssCustomClass += 'table.dxgvTable_base {font-size: 10px; }';
		cssCustomClass += 'td.dxgv {width:30px; padding:1px 5px !important; white-space: nowrap; text-align:center;}';
	  	cssCustomClass += 'td.dxgvHEC {display:none;}';
	  	cssCustomClass += 'table.dxgvRBB {table-layout:auto !important;}';

	  	cssCustomClass += '.currentDayHeader {background-color: gray !important;}';
	  	cssCustomClass += '.currentDayHeader td {color: #fff !important; font-weight:bold;}';
	  	cssCustomClass += '.currentDayColumn {border-right:1px solid gray !important;}';
	  	cssCustomClass += '.dxgvSelectedRow_base {background-color:#e0e0e0 !important;}';

	  	cssCustomClass += '.cellFesta {background-color: #f67b7ba6 !important; color: black !important;}';
		cssCustomClass += '.cellA, .cellAP, .cellAF, .cellAA, .cellA_A {background-color: #f67b7ba6}';
		cssCustomClass += '.cellA_P, .cellAP_P {background-image: linear-gradient(90deg, #f67b7ba6, #61c161a6)}';
		cssCustomClass += '.cellP_A, .cellP_AP {background-image: linear-gradient(90deg, #61c161a6, #f67b7ba6)}';
		cssCustomClass += '.cellP {background-color: #61c161a6 !important}';
		cssCustomClass += '.cellSW {background-color: #004bff5e}';
		cssCustomClass += '.cellSW_A, .cellSW_AP, .cellSW_AA {background-image: linear-gradient(90deg, #004bff5e, #f67b7ba6)}';
		cssCustomClass += '.cellA_SW, .cellAP_SW, .cellAA_SW {background-image: linear-gradient(90deg, #f67b7ba6, #004bff5e)}';
		cssCustomClass += '.cellSW_P {background-image: linear-gradient(90deg, #004bff5e, #61c161a6)}';
		cssCustomClass += '.cellP_SW {background-image: linear-gradient(90deg, #61c161a6, #004bff5e)}';
	  	cssCustomClass += '.cellT, .cellT_P, .cellP_T, .cellA_T, .cellT_A {background-color: #ffef005e}';

		$('<style>' + cssCustomClass + '</style>').appendTo('head');
	};

	self.addUncolorizeButton = function(){
		$('input[type="submit"][value="Ricerca"]').closest('td').prepend(tagBtnUncolorize);
		$('#btnUncolorize').click(self.uncolorize);
	};

	self.uncolorize = function(){
	  $('#ctl00_ctMain_grd_ReportAdminPresenze_DXMainTable tbody tr td[class*="cell"]')
			.removeClass()
	  		.addClass('dxgv');
	};

	self.colorize = function(){
		  $('#ctl00_ctMain_grd_ReportAdminPresenze_DXMainTable tbody tr td[style*="background-color:Black"]')
	  		.addClass('cellFesta');

		  $('#ctl00_ctMain_grd_ReportAdminPresenze_DXMainTable tbody tr td').each(function(){
	  		var content = $(this).text().replace(' ', '_');
	  		$(this).addClass('cell' + content);
		});
	  	var currentMonth = $('#ctl00_ctMain_txt_Mese_I').val();
	  	var currentYear = $('#ctl00_ctMain_txt_Anno_I').val();
	  	var today = new Date();
	  	var currentDay = today.getDate();

	  	if(today.getFullYear() == currentYear && (today.getMonth() + 1) == currentMonth){

			$('td.cell' + currentDay).closest('td.dxgvHeader_base').addClass('currentDayHeader');
		  	$('table.colorizzato tbody tr.dxgvDataRow_base').each(function(){
				$(this).find('td').eq(currentDay-1).addClass('currentDayColumn');
			  	$(this).find('td').eq(currentDay).addClass('currentDayColumn');
			});
		};
	};
};

window.addEventListener('load', function () {
	new TrueTimeHack.PresenzeManager().initialize();
}, true);