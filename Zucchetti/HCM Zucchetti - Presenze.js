// ==UserScript==
// @name         HCM Zucchetti - Presenze
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @require			 http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @match        https://saas.hrzucchetti.it/WFINFOCAMERE/servlet/hfpr_bcapcarte*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=hrzucchetti.it
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

		var jq = jQuery.noConflict();

		var eventiDaCaricare = true;

		var waitUntilLoad = function(){
			setTimeout(waitUntil, 800);
		}

		// Ridefinisce le dimensioni della tabella del calendario e dei contenuti
		var waitUntil = function(){

			// Ridefinisce la densità della tabella
			var table = jq('table[id$="_Grid1"]');
			jq(table).removeAttr('width');
			jq(table).find('td')
				.removeAttr('data-user-width')
				.attr('style', 'font-size: 10px; padding-top:2px !important; padding-bottom:2px !important');
			jq(table).find('td.richieste').css('width', '300px');
			jq(table).find('tr').removeAttr('style');
			jq(table).find('div.fakeRow div span').removeAttr('style');
			jq(table).find('div.fakeTable, div.fakeRow').attr('style', 'width:inherit; white-space:nowrap');
			jq(table).find('div.fakeRow').attr('style', 'display:block');
			jq(table).find('div.fakeRow div.statusCell').siblings('span').attr('style', 'float:none; display:inline');

			// Aggiunge spazio in coda alla tabella per centrarla e aumenta la dimensione del div contenitore per far sparire le scrollbar
			jq('table.pagelet_body').append('<div style="height:100px"></div>');
			jq(table).parent('div').removeAttr('style');

			// Individua le righe dei festivi
			var tdFestivi = jq('table[id$="_Grid1"]').find('span[title="Sabato"],span[title="Domenica"],span[title="Festivo"]');

			if(tdFestivi.length == 0){
				window.setTimeout(waitUntil,500); // Il ritardo per dare tempo alla pagina di caricare la tabella
				return;
			}

			for(var i=0; i<tdFestivi.length; i++){
				// Evidenzia tutti i festivi
				var riga = jq(tdFestivi[i]).closest('tr');

					riga.css('background-color', '#fbd9d9');
					//jq(riga.find('span:first-child')[0])
					jq(riga.find('span:first-child'))
						//.css('color', '')
						.attr('style', 'color:red !important; font-weight:bold');
						//.css('background-color', 'red');
			}

			if(eventiDaCaricare){
				// Nel caso di click sui vari link rilancia nuovamente la funzione
				eventiDaCaricare = false;
				jq('select[name="TxtMese"]').on('change', function (){setTimeout(waitUntil,500);});
				jq('select[name="TxtAnno"]').on('change', function (){setTimeout(waitUntil,500);});
				jq('a.BtnMesePrev_ctrl').on('click', function (){setTimeout(waitUntil,500);});
				jq('a.BtnMeseNext_ctrl').on('click', function (){setTimeout(waitUntil,500);});
				jq('a[id$="_FoglioPresenze_container_TAB_href"]').on('click', function(){setTimeout(waitUntil,500);});
				jq('a[id$="_Cartellino_container_TAB_href"]').on('click', function(){setTimeout(waitUntil,500);});

				// Scroll automatico fino al punto interessante della pagina
				jq('select.TxtMese_ctrl').get(0).scrollIntoView();
			}
		}



		waitUntil();

})();