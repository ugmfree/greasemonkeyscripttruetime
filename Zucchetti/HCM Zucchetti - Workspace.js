// ==UserScript==
// @name         HCM Zucchetti - Workspace
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @require			 http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js
// @match        https://saas.hrzucchetti.it/hrpinfocamere/jsp/home.jsp*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=hrzucchetti.it
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

		var jq = jQuery.noConflict();

		// Aggiunta di link nel footer
		jq('div.tabApplication div div.tab_center_text').append("<a style='color:white' href='https://saas.hrzucchetti.it/WFINFOCAMERE/servlet/hfpr_bcapcarte' target='_blank'>Presenze</a>");
		jq('div.tabApplication div div.tab_center_text').append("<a style='color:white' href='https://saas.hrzucchetti.it/WFINFOCAMERE/servlet/hfma_bviscma' target='_blank'>Cruscotto mensile</a>");

})();