// ==UserScript==
// @name         HCM Zucchetti - Richiesta ferie e SM
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://saas.hrzucchetti.it/WFINFOCAMERE/jsp/hfpr_layercart.jsp?Titolo=S&TAB=Giustificativo*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=hrzucchetti.it
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

		var jq = jQuery.noConflict();

		// Move smart working and ferie option to the top
		var changeDropdown = function(){
			var ddl = jq('select[name="Combobox23"]');
			var options = jq(ddl).find('option');


			jq(options[5]).insertAfter(jq(options[0]));
			jq(options[25]).insertAfter(jq(options[0]));

			jq(ddl).find('option[value="1SW"]')
				.prop('selected', true)
				.trigger("change");

			setTimeout(clickOnCheckbox, 500);
		}

		// Click on giorno intero checkbox
		var clickOnCheckbox = function(){
			jq('input[name="cPeriodoBox"]').trigger('click');
		}

		setTimeout(changeDropdown, 500);
})();